class CreateMoney < ActiveRecord::Migration[5.2]
  def up
    create_table :money do |t|
      t.integer :nomination
      t.integer :amount
    end
  end

  def down
    drop_table :money
  end
end
