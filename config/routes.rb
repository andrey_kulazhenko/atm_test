Rails.application.routes.draw do
  scope :api do
    scope :v1 do
      scope :money do
        post 'add', to: 'money#adding'
        post 'withdraw', to: 'money#withdrawal'
      end
    end
  end
end
