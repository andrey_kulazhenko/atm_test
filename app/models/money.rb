# frozen_string_literal: true

class Money < ApplicationRecord
  validates :amount, :nomination, presence: true
  validates :amount, numericality: { greater_than_or_equal_to: 0 }
end
