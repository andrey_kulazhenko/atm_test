# frozen_string_literal: true

class MoneyController < ApplicationController
  rescue_from StandardError, with: :error_handler

  def adding
    result = MoneyService.add_money adding_params
    render json: result, status: :ok
  end

  def withdrawal
    result = MoneyService.withdraw_money withdrawal_params
    render json: result, status: :ok
  end

  private

  def adding_params
    params.require(:adding).permit!
  end

  def withdrawal_params
    params.require(:withdrawal).permit(:value)
  end

  def error_handler(exception)
    render json: { message: exception }, status: :unprocessable_entity
  end
end
