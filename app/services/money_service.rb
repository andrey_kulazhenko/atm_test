# frozen_string_literal: true

class MoneyService
  class << self
    def add_money(params)
      params.each do |nomination, amount|
        amount = amount.to_i
        model = Money.find_by nomination: nomination
        raise StandardError, 'Wrong nomination provided' unless model
        raise StandardError, 'Wrong amount value' if amount <= 0

        model.increment! :amount, amount
      end

      prepare Money.all
    end

    def withdraw_money(params)
      value = params['value'].to_i
      raise StandardError, 'Wrong amount value' if value <= 0

      withdrawing = []
      Money.transaction do
        while value.positive?
          item = Money.where('nomination <= ? and amount > ?', value, 0).order(nomination: :desc).first
          raise StandardError, 'Not enough money in ATM for operation' unless item

          count = value / item.nomination
          count = item.amount if item.amount < count

          item.decrement! :amount, count
          withdrawing << Money.new(nomination: item.nomination, amount: count)
          value -= item.nomination * count
        end
      end

      prepare withdrawing
    end

    private

    def prepare(items)
      result = {}
      items.map { |item| result[item.nomination] = item.amount }
      result
    end
  end
end
