require 'rails_helper'

RSpec.describe MoneyController do
  describe 'POST add' do
    before(:all) do
      [1, 2, 5, 10, 25, 50].each { |nom| create(:money, nomination: nom) }
    end

    it 'should be successful' do
      post :adding, params: {
        adding: { '1': 4, '5': 2 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(200)
      expect(hash_body).to include('1', '2', '5', '10', '25', '50')
    end

    it 'should raise Wrong amount error' do
      post :adding, params: {
        adding: { '1': 0, '5': 2 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(422)
      expect(hash_body).to include(message: 'Wrong amount value')
    end

    it 'should raise Wrong nomination provided' do
      post :adding, params: {
        adding: { '1111': 0, '5': 2 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(422)
      expect(hash_body).to include(message: 'Wrong nomination provided')
    end
  end

  describe 'POST withdraw' do
    before(:all) do
      [1, 2, 5, 10, 25, 50].each { |nom| create(:money, nomination: nom) }
    end

    it 'should be successful' do
      post :withdrawal, params: {
        withdrawal: { value: 65 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(200)
      expect(hash_body).to include('50', '10', '5')
    end

    it 'should raise Wrong amount value' do
      post :withdrawal, params: {
        withdrawal: { value: 0 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(422)
      expect(hash_body).to include(message: 'Wrong amount value')
    end

    it 'should raise Not enough money in ATM for operation' do
      post :withdrawal, params: {
        withdrawal: { value: 100500 }
      }
      hash_body = JSON.parse(response.body).with_indifferent_access
      expect(response.status).to eql(422)
      expect(hash_body).to include(message: 'Not enough money in ATM for operation')
    end
  end
end
