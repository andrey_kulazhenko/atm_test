FactoryBot.define do
  factory :money do
    amount { rand(1..5) }
    nomination { [1, 2, 5, 10, 25, 25, 50].sample }
  end
end
